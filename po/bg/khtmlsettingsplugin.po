# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Zlatko Popov <zlatkopopov@fsa-bg.org>, 2006, 2007.
# Aleksandar Yordanov <punkin.drublik@gmail.com>, 2007.
# Yasen Pramatarov <yasen@lindeas.com>, 2009, 2011, 2021.
msgid ""
msgstr ""
"Project-Id-Version: khtmlsettingsplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-20 01:59+0000\n"
"PO-Revision-Date: 2021-01-07 11:31+0200\n"
"Last-Translator: Yasen Pramatarov <yasen@lindeas.com>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. i18n: ectx: Menu (tools)
#: khtmlsettingsplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "&Инструменти"

#. i18n: ectx: ToolBar (extraToolBar)
#: khtmlsettingsplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "Допълнителна лента с инструменти"

#: settingsplugin.cpp:43
#, kde-format
msgid "HTML Settings"
msgstr "Настройки на HTML"

#: settingsplugin.cpp:48
#, kde-format
msgid "Java&Script"
msgstr "Java&Script"

#: settingsplugin.cpp:53
#, kde-format
msgid "&Java"
msgstr "&Java"

#: settingsplugin.cpp:58
#, kde-format
msgid "&Cookies"
msgstr "&Бисквитки"

#: settingsplugin.cpp:63
#, kde-format
msgid "&Plugins"
msgstr "&Приставки"

#: settingsplugin.cpp:68
#, kde-format
msgid "Autoload &Images"
msgstr "&Автоматично зареждане на изображения"

#: settingsplugin.cpp:75
#, kde-format
msgid "Enable Pro&xy"
msgstr "Включване на п&роксито"

#: settingsplugin.cpp:80
#, kde-format
msgid "Enable Cac&he"
msgstr "Включване на к&еш-паметта"

#: settingsplugin.cpp:85
#, kde-format
msgid "Cache Po&licy"
msgstr "Правила на ке&ш-паметта"

#: settingsplugin.cpp:87
#, kde-format
msgid "&Keep Cache in Sync"
msgstr "&Синхронизиране на кеш-паметта"

#: settingsplugin.cpp:88
#, kde-format
msgid "&Use Cache if Possible"
msgstr "&Използване кеш-паметта, когато е възможно"

#: settingsplugin.cpp:89
#, kde-format
msgid "&Offline Browsing Mode"
msgstr "Ре&жим \"Без връзка с мрежата\""

#: settingsplugin.cpp:193
#, kde-format
msgid ""
"The cookie setting could not be changed, because the cookie daemon could not "
"be contacted."
msgstr ""
"Бисквитките не могат да бъдат променени, понеже няма връзка с демона им."

#: settingsplugin.cpp:195
#, kde-format
msgctxt "@title:window"
msgid "Cookie Settings Unavailable"
msgstr "Насткойките на бисквитки са недостъпни"
